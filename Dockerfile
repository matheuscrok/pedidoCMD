FROM openjdk:21-jdk-slim AS build

COPY src /app/src
COPY pom.xml /app

WORKDIR /app

RUN apt-get update && apt-get install -y maven

RUN mvn clean package  -DskipTests -Dspring.profiles.active=prod

FROM openjdk:21-jdk-slim AS runtime

WORKDIR /app

COPY --from=build /app/target/*.jar /app/cmd_pedido.jar

EXPOSE 8084
ENV SPRING_PROFILES_ACTIVE=prod
ENTRYPOINT ["java", "-Dspring.profiles.active=prod", "-jar", "cmd_pedido.jar"]
