package com.service.apicmdpedido;

import com.service.apicmdpedido.controle.ControlePedido;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;
import com.service.apicmdpedido.negocio.NegocioPedido;
import com.service.apicmdpedido.utilidades.excecoes.NumeroControleExistenteException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = PedidoCmdApplication.class)
@ActiveProfiles("test")
class PedidoIntegracaoControllerTests {

    @Autowired
    private ControlePedido controlePedido;

    @MockBean
    private NegocioPedido negocioPedido;

    @Test
    void testCriarPedidoControladorRespostaCreated() {
        List<ReqDetalhePedidoDTO> detalhesPedido = Collections.singletonList(
                new ReqDetalhePedidoDTO(null, 10, BigDecimal.valueOf(100), "Produto"));

        ReqPedidoDTO novoPedidoDTO = new ReqPedidoDTO(null, LocalDate.now(), null, 1L, detalhesPedido);
        ResPedidoDTO resultadoEsperado = new ResPedidoDTO(null, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());

        when(negocioPedido.criarPedido(novoPedidoDTO, negocioPedido))
                .thenReturn(resultadoEsperado);

        ResponseEntity<?> resposta = controlePedido.criarPedido(novoPedidoDTO);

        assertEquals(HttpStatus.CREATED, resposta.getStatusCode());
    }


    @Test
    void testCriarPedidoControladorRespostaBadRequest() {
        List<ReqDetalhePedidoDTO> detalhesPedido = Collections.singletonList(
                new ReqDetalhePedidoDTO(null, 10, BigDecimal.valueOf(100), "Produto")
        );
        ReqPedidoDTO novoPedidoDTO = new ReqPedidoDTO(null, LocalDate.now(), null, 1L, detalhesPedido);

        when(negocioPedido.criarPedido(any(ReqPedidoDTO.class), any(NegocioPedido.class)))
                .thenThrow(new NumeroControleExistenteException("Número de controle já cadastrado."));

        ResponseEntity<?> resposta = controlePedido.criarPedido(novoPedidoDTO);
        assertEquals(HttpStatus.BAD_REQUEST, resposta.getStatusCode());
    }

    @Test
    void testCriarPedidoControladorRespostaInternalServerErrorNumeroControle() {
        List<ReqDetalhePedidoDTO> detalhesPedido = Collections.singletonList(
                new ReqDetalhePedidoDTO(null, 10, BigDecimal.valueOf(100), "Produto")
        );

        ReqPedidoDTO novoPedidoDTO = new ReqPedidoDTO(null, LocalDate.now(), null, 1L, detalhesPedido);

        ResPedidoDTO resultadoEsperado = new ResPedidoDTO(null, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());
        CompletableFuture<ResPedidoDTO> futureResult = CompletableFuture.completedFuture(resultadoEsperado);

        when(negocioPedido.criarPedido(any(ReqPedidoDTO.class), any(NegocioPedido.class))).thenAnswer(invocation -> futureResult);

        ResponseEntity<?> resposta = controlePedido.criarPedido(novoPedidoDTO);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, resposta.getStatusCode());
    }

    @Test
    void testCriarPedidoControladorRespostaInternalServerError() {
        ReqPedidoDTO novoPedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());

        when(negocioPedido.criarPedido(any(ReqPedidoDTO.class), any(NegocioPedido.class)))
                .thenThrow(new RuntimeException("Erro inesperado"));

        ResponseEntity<?> resposta = controlePedido.criarPedido(novoPedidoDTO);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, resposta.getStatusCode(), "Uma exceção inesperada deve resultar em um status INTERNAL_SERVER_ERROR.");
    }

}
