package com.service.apicmdpedido;

import com.service.apicmdpedido.entidade.dto.requisicao.ReqDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.negocio.NegocioPedido;
import com.service.apicmdpedido.repositorio.RepositorioPedido;
import com.service.apicmdpedido.utilidades.excecoes.CodigoClienteInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.DetalhePedidoInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.NumeroControleExistenteException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = PedidoCmdApplication.class)
@ActiveProfiles("test")
class PedidoIntegracaoValidationTests {

    @Mock
    private RepositorioPedido repositorioPedido;

    @InjectMocks
    private NegocioPedido negocioPedido;
    private ReqPedidoDTO pedidoDTO;

    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }
    @Test
    void testExistsByNumeroControle() {
        Long numeroControleExistente = 123L;
        Long numeroControleNaoExistente = 456L;

        when(repositorioPedido.existsByNumeroControle(numeroControleExistente)).thenReturn(true);
        when(repositorioPedido.existsByNumeroControle(numeroControleNaoExistente)).thenReturn(false);

        assertTrue(negocioPedido.numeroControleExiste(numeroControleExistente));
        assertFalse(negocioPedido.numeroControleExiste(numeroControleNaoExistente));
    }

    @Test
    void testCriarPedidoComNumeroControleDuplicado() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());

        when(repositorioPedido.existsByNumeroControle(123L)).thenReturn(true);

        assertThrows(NumeroControleExistenteException.class, () -> negocioPedido.criarPedido(pedidoDTO, negocioPedido));
    }

    @Test
    void testCriarPedidoComItensExcedentes() {
        List<ReqDetalhePedidoDTO> detalhesExcedentes = Collections.nCopies(11, new ReqDetalhePedidoDTO(1L, 1, BigDecimal.valueOf(100), "Produto"));
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1100), 1L, detalhesExcedentes);

        assertThrows(DetalhePedidoInvalidoException.class, () -> negocioPedido.criarPedido(pedidoDTO, negocioPedido));
    }

    @Test
    void testCriarPedidoComCodigoClienteInvalido() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 0L, Collections.emptyList());

        assertThrows(CodigoClienteInvalidoException.class, () -> negocioPedido.criarPedido(pedidoDTO, negocioPedido));
    }

}