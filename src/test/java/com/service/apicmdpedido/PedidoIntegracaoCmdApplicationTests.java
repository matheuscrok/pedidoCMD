package com.service.apicmdpedido;

import com.service.apicmdpedido.entidade.Pedido;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;
import com.service.apicmdpedido.negocio.NegocioPedido;
import com.service.apicmdpedido.repositorio.RepositorioPedido;
import com.service.apicmdpedido.utilidades.excecoes.CodigoClienteInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.NumeroControleExistenteException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = PedidoCmdApplication.class)
@ActiveProfiles("test")
class PedidoIntegracaoCmdApplicationTests {

    @MockBean
    private RepositorioPedido repositorioPedido;
    @InjectMocks
    private NegocioPedido negocioPedido;
    private ReqPedidoDTO pedidoDTO;

    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }
    @Test
    void testCriarPedidoComCodigoClienteInvalido() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 0L, Collections.emptyList());

        assertThrows(CodigoClienteInvalidoException.class, () -> this.negocioPedido.criarPedido(pedidoDTO, negocioPedido));
    }

    @Test
    void testCriarPedidoComDadosPadrao() {
        List<ReqDetalhePedidoDTO> detalhes = Collections.singletonList(new ReqDetalhePedidoDTO(1L, null, BigDecimal.valueOf(100), "Produto"));
        pedidoDTO = new ReqPedidoDTO(null, null, BigDecimal.valueOf(100), 1L, detalhes);

        when(repositorioPedido.save(any(Pedido.class))).thenReturn(new Pedido(123L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList()));
        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);
        ResPedidoDTO resultado = negocioPedido.criarPedido(pedidoDTO, negocioPedido);

        assertNotNull(resultado.dataCadastro());
        assertEquals(LocalDate.now(), resultado.dataCadastro());
        resultado.detalhes().forEach(detalhe -> assertEquals(1, detalhe.quantidade()));
    }


    @Test
    void testCriacaoPedidoComNumeroControleDuplicadoIntegracao() {
        ReqPedidoDTO pedidoDuplicadoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(true);

        assertThrows(NumeroControleExistenteException.class, () -> negocioPedido.criarPedido(pedidoDuplicadoDTO, negocioPedido));
    }

    @Test
    void testCriarPedidoComDataCadastroPadrao() {
        List<ReqDetalhePedidoDTO> detalhesPedido = Collections.emptyList();
        pedidoDTO = new ReqPedidoDTO(123L, null, BigDecimal.valueOf(1000), 1L, detalhesPedido);

        Pedido pedidoMock = new Pedido(123L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList());

        assertNotNull(pedidoMock);

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);
        when(repositorioPedido.save(any(Pedido.class))).thenReturn(pedidoMock);

        ResPedidoDTO resultado = negocioPedido.criarPedido(pedidoDTO, negocioPedido);

        assertNotNull(resultado.dataCadastro(), "A data de cadastro não deve ser nula.");
        assertEquals(LocalDate.now(), resultado.dataCadastro(), "A data de cadastro deve ser a data atual quando não fornecida.");
    }


}
