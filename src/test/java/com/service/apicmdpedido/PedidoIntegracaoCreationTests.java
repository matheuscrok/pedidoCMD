package com.service.apicmdpedido;

import com.service.apicmdpedido.entidade.Pedido;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;
import com.service.apicmdpedido.negocio.NegocioPedido;
import com.service.apicmdpedido.repositorio.RepositorioPedido;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = PedidoCmdApplication.class)
@ActiveProfiles("test")
class PedidoIntegracaoCreationTests {

    @MockBean
    private RepositorioPedido repositorioPedido;

    @InjectMocks
    private NegocioPedido negocioPedido;
    @MockBean
    private KafkaTemplate<String, String> kafkaTemplate;

    private ReqPedidoDTO pedidoDTO;

    private boolean setUpNecessario = false;

    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void testCriarPedidoValido() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());
        when(repositorioPedido.save(any(Pedido.class))).thenReturn(new Pedido(123L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList()));
        BigDecimal valorTotal = BigDecimal.valueOf(1000);
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), valorTotal, 1L, Collections.emptyList());

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);

        ResPedidoDTO resultado = negocioPedido.criarPedido(pedidoDTO, negocioPedido);
        assertNotNull(resultado);

        verify(repositorioPedido, times(1)).save(any(Pedido.class));
        verify(kafkaTemplate, times(1)).send(anyString(), anyString());
    }

    @Test
    void testCriacaoPedidoPersistenciaBancoDados() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());
        when(repositorioPedido.save(any(Pedido.class))).thenReturn(new Pedido(123L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList()));
        ReqPedidoDTO novoPedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.singletonList(new ReqDetalhePedidoDTO(1L, 2, BigDecimal.valueOf(500), "Produto Teste")));

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);

        ResPedidoDTO resultado = negocioPedido.criarPedido(novoPedidoDTO, negocioPedido);

        assertNotNull(resultado);
        assertEquals(123L, resultado.numeroControle());
        assertNotNull(resultado.dataCadastro());

        verify(repositorioPedido, times(1)).save(any(Pedido.class));
    }

    @Test
    void testCriarPedidoCalculoValorTotal() {
        pedidoDTO = new ReqPedidoDTO(123L, LocalDate.now(), BigDecimal.valueOf(1000), 1L, Collections.emptyList());
        when(repositorioPedido.save(any(Pedido.class))).thenReturn(new Pedido(123L, LocalDate.now(), 1L, BigDecimal.valueOf(1000), Collections.emptyList()));
        List<ReqDetalhePedidoDTO> detalhesSemDesconto = Collections.singletonList(new ReqDetalhePedidoDTO(1L, 4, BigDecimal.valueOf(100), "Produto"));
        List<ReqDetalhePedidoDTO> detalhesComDesconto = Collections.singletonList(new ReqDetalhePedidoDTO(1L, 10, BigDecimal.valueOf(100), "Produto"));

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);

        BigDecimal totalSemDescontoCalculado = negocioPedido.calcularValorTotal(detalhesSemDesconto);
        BigDecimal totalComDescontoCalculado = negocioPedido.calcularValorTotal(detalhesComDesconto);

        assertNotNull(totalSemDescontoCalculado);
        assertNotNull(totalComDescontoCalculado);

        assertEquals(0, BigDecimal.valueOf(400).compareTo(totalSemDescontoCalculado));
        assertEquals(0, BigDecimal.valueOf(900).compareTo(totalComDescontoCalculado));
    }

    @Test
    void testCriacaoPedidoComDescontoIntegracao() {
        List<ReqDetalhePedidoDTO> detalhesComDesconto = Collections.singletonList(
                new ReqDetalhePedidoDTO(null, 10, BigDecimal.valueOf(100), "Produto")
        );

        ReqPedidoDTO pedidoDTO = new ReqPedidoDTO(null, LocalDate.now(), null, 1L, detalhesComDesconto);

        when(repositorioPedido.save(any(Pedido.class))).thenAnswer(i -> i.getArgument(0));

        when(repositorioPedido.existsByNumeroControle(anyLong())).thenReturn(false);

        ResPedidoDTO resultado = negocioPedido.criarPedido(pedidoDTO, negocioPedido);

        assertNotNull(resultado, "O resultado não deve ser nulo");
        assertEquals(0, BigDecimal.valueOf(900.0).compareTo(resultado.valorTotal()), "O valor total deve ser 900.0");
    }


}
