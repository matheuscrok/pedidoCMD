CREATE DATABASE IF NOT EXISTS microservico_pedido_cmd;

USE microservico_pedido_cmd;


CREATE TABLE IF NOT EXISTS pedido
(
    numero_controle BIGINT PRIMARY KEY,
    data_cadastro   DATE           NOT NULL,
    codigo_cliente  BIGINT         NOT NULL,
    valor_total     DECIMAL(10, 2) NOT NULL
);


CREATE TABLE IF NOT EXISTS detalhe_pedido
(
    id         BIGINT PRIMARY KEY AUTO_INCREMENT,
    quantidade INT    NOT NULL DEFAULT 1,
    preco_unitario DECIMAL(10, 2) NOT NULL,
    nome_produto   VARCHAR(255)   NOT NULL,
    pedido     BIGINT NOT NULL,
    FOREIGN KEY (pedido) REFERENCES pedido (numero_controle)
);
