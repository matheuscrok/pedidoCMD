package com.service.apicmdpedido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PedidoCmdApplication {

    public static void main(String[] args) {
        SpringApplication.run(PedidoCmdApplication.class, args);
    }
}
