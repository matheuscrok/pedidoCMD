package com.service.apicmdpedido.negocio;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.service.apicmdpedido.config.fallback.FallbackService;
import com.service.apicmdpedido.config.fallback.PortChecker;
import com.service.apicmdpedido.entidade.DetalhePedido;
import com.service.apicmdpedido.entidade.Pedido;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;
import com.service.apicmdpedido.repositorio.RepositorioPedido;
import com.service.apicmdpedido.utilidades.DTOConverter;
import com.service.apicmdpedido.utilidades.excecoes.CodigoClienteInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.DetalhePedidoInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.JsonConversionException;
import com.service.apicmdpedido.utilidades.excecoes.NumeroControleExistenteException;
import jakarta.transaction.Transactional;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class NegocioPedido {
    private static final Logger log = LoggerFactory.getLogger(NegocioPedido.class);

    private RepositorioPedido pedidoRepository;

    private KafkaTemplate<String, String> kafkaTemplate;
    private FallbackService fallbackService;

    public NegocioPedido(RepositorioPedido pedidoRepository, KafkaTemplate<String, String> kafkaTemplate, FallbackService fallbackService) {
        this.pedidoRepository = pedidoRepository;
        this.kafkaTemplate = kafkaTemplate;
        this.fallbackService = fallbackService;
    }

    public void enviarPedidoMicroservico(ResPedidoDTO resPedidoDTO) {
        try {
            String mensagemJson = convertToJson(resPedidoDTO);
            if (PortChecker.isPortInUse("localhost", 9092) || PortChecker.isPortInUse("kafka", 9092)) {
                    kafkaTemplate.send("pedido-events", mensagemJson);
            } else {
                enviarPorCQRS(resPedidoDTO);
            }
        } catch (Exception e) {
            log.error("Erro ao enviar mensagem para Kafka: " + e.getMessage(), e);
            enviarPorCQRS(resPedidoDTO);
        }
    }

    private void enviarPorCQRS(ResPedidoDTO resPedidoDTO) {
        try {
            fallbackService.enviarPedido(new Pedido(resPedidoDTO.numeroControle(), resPedidoDTO.dataCadastro(), resPedidoDTO.codigoCliente(), resPedidoDTO.valorTotal(), resPedidoDTO.detalhes().stream()
                    .map(d -> new DetalhePedido(d.id(), (d.quantidade() != null && d.quantidade() > 0)  ? d.quantidade() : 1, d.precoUnitario(), d.nomeProduto(), new Pedido(resPedidoDTO.numeroControle(), resPedidoDTO.dataCadastro(), resPedidoDTO.codigoCliente(), resPedidoDTO.valorTotal(), new ArrayList<>())))
                    .collect(Collectors.toList())));
        } catch (Exception e) {
            log.error("Erro ao enviar mensagem Kafka: " + e.getMessage(), e);
        }
    }

    private String convertToJson(ResPedidoDTO obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new JsonConversionException("Erro ao converter objeto para JSON", e);
        }
    }


    public boolean numeroControleExiste(Long numeroControle) {
        return pedidoRepository.existsByNumeroControle(numeroControle);
    }

    @Transactional
    public ResPedidoDTO criarPedido(ReqPedidoDTO reqPedidoDTO, NegocioPedido negocioPedido) {
        validarPedido(reqPedidoDTO);
        LocalDate dataCadastro = reqPedidoDTO.dataCadastro() != null ? reqPedidoDTO.dataCadastro() : LocalDate.now();
        BigDecimal valorTotal = calcularValorTotal(reqPedidoDTO.detalhes());

        Pedido pedido = new Pedido(reqPedidoDTO.numeroControle(), dataCadastro, reqPedidoDTO.codigoCliente(), valorTotal, reqPedidoDTO.detalhes().stream()
                .map(d -> new DetalhePedido(d.id(), (d.quantidade() != null && d.quantidade() > 0)  ? d.quantidade() : 1, d.precoUnitario(), d.nomeProduto(), new Pedido(reqPedidoDTO.numeroControle(), dataCadastro, reqPedidoDTO.codigoCliente(), valorTotal, new ArrayList<>())))
                .collect(Collectors.toList()));

        pedido = pedidoRepository.save(pedido);


        ResPedidoDTO resPedidoDTO = DTOConverter.convertPedidoEntityToDTO(pedido);
        if (pedido != null) {
            enviarPedidoMicroservico(resPedidoDTO);
        }
        return resPedidoDTO;
    }


    public BigDecimal calcularValorTotal(List<ReqDetalhePedidoDTO> detalhes) {
        return detalhes.stream().map(detalhe -> {
            int quantidade = detalhe.quantidade() != null && detalhe.quantidade() > 0 ? detalhe.quantidade() : 1;
            BigDecimal subtotal = detalhe.precoUnitario().multiply(BigDecimal.valueOf(quantidade));

            BigDecimal descontoPercentual = BigDecimal.ZERO;
            if (quantidade > 5) {
                descontoPercentual = (quantidade >= 10) ? BigDecimal.valueOf(0.10) : BigDecimal.valueOf(0.05);
            }

            return subtotal.subtract(subtotal.multiply(descontoPercentual));
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
    }


    private void validarPedido(ReqPedidoDTO reqPedidoDTO) {
        if (reqPedidoDTO.detalhes().size() > 10) {
            throw new DetalhePedidoInvalidoException("Máximo de 10 itens por pedido.");
        }

        if (reqPedidoDTO.codigoCliente() < 1 || reqPedidoDTO.codigoCliente() > 10) {
            throw new CodigoClienteInvalidoException("Código do cliente inválido.");
        }

        if (numeroControleExiste(reqPedidoDTO.numeroControle())) {
            throw new NumeroControleExistenteException("Número de controle já cadastrado.");
        }

    }
}
