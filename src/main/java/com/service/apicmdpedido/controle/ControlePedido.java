package com.service.apicmdpedido.controle;

import com.service.apicmdpedido.entidade.dto.requisicao.ReqPedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;
import com.service.apicmdpedido.negocio.NegocioPedido;
import com.service.apicmdpedido.utilidades.excecoes.DetalhePedidoInvalidoException;
import com.service.apicmdpedido.utilidades.excecoes.NumeroControleExistenteException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pedidos")
@CrossOrigin(origins = "*")
public class ControlePedido {

    private NegocioPedido negocioPedido;

    public ControlePedido(NegocioPedido negocioPedido) {
        this.negocioPedido = negocioPedido;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Object> criarPedido(@RequestBody ReqPedidoDTO reqPedidoDTO) {
        try {
            ResPedidoDTO novoPedido = negocioPedido.criarPedido(reqPedidoDTO, negocioPedido);
            return new ResponseEntity<>(novoPedido, HttpStatus.CREATED);
        } catch (NumeroControleExistenteException | DetalhePedidoInvalidoException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}