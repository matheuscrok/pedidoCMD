package com.service.apicmdpedido.entidade.dto.requisicao;

import java.math.BigDecimal;

public record ReqDetalhePedidoDTO(Long id, Integer quantidade, BigDecimal precoUnitario, String nomeProduto) {
}