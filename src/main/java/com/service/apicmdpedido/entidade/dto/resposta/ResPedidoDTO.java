package com.service.apicmdpedido.entidade.dto.resposta;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public record ResPedidoDTO(Long numeroControle, LocalDate dataCadastro, BigDecimal valorTotal,
                           Long codigoCliente, List<ResDetalhePedidoDTO> detalhes) {
}