package com.service.apicmdpedido.entidade.dto.resposta;

import java.math.BigDecimal;

public record ResDetalhePedidoDTO(Long id, Integer quantidade, BigDecimal precoUnitario, String nomeProduto) {
}