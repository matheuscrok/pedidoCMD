package com.service.apicmdpedido.entidade.dto.requisicao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public record ReqPedidoDTO(Long numeroControle, LocalDate dataCadastro, BigDecimal valorTotal,
                           Long codigoCliente, List<ReqDetalhePedidoDTO> detalhes) {
}