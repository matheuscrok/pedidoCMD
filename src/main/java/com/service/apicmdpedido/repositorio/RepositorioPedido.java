package com.service.apicmdpedido.repositorio;

import com.service.apicmdpedido.entidade.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioPedido extends JpaRepository<Pedido, Long> {
    boolean existsByNumeroControle(Long numeroControle);
}
