package com.service.apicmdpedido.utilidades;

import com.service.apicmdpedido.entidade.DetalhePedido;
import com.service.apicmdpedido.entidade.Pedido;
import com.service.apicmdpedido.entidade.dto.resposta.ResDetalhePedidoDTO;
import com.service.apicmdpedido.entidade.dto.resposta.ResPedidoDTO;

import java.util.List;
import java.util.stream.Collectors;

public class DTOConverter {

    private DTOConverter() {
    }

    public static ResPedidoDTO convertPedidoEntityToDTO(Pedido pedido) {
        List<ResDetalhePedidoDTO> detalhesDTO = pedido.getDetalhes().stream()
                .map(DTOConverter::convertDetalhePedidoEntityToDTO)
                .collect(Collectors.toList());
        return new ResPedidoDTO(pedido.getNumeroControle(), pedido.getDataCadastro(), pedido.getValorTotal(),
                pedido.getCodigoCliente(), detalhesDTO);
    }

    public static ResDetalhePedidoDTO convertDetalhePedidoEntityToDTO(DetalhePedido detalhePedido) {
        return new ResDetalhePedidoDTO(detalhePedido.getId(), detalhePedido.getQuantidade(),
                detalhePedido.getPrecoUnitario(), detalhePedido.getNomeProduto());
    }
}
