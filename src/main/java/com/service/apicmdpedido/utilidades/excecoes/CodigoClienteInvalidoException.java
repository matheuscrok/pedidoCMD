package com.service.apicmdpedido.utilidades.excecoes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodigoClienteInvalidoException extends RuntimeException {
    public CodigoClienteInvalidoException(String message) {
        super(message);
    }
}
