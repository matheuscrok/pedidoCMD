package com.service.apicmdpedido.utilidades.excecoes;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NumeroControleExistenteException extends RuntimeException {
    public NumeroControleExistenteException(String message) {
        super(message);
    }
}



