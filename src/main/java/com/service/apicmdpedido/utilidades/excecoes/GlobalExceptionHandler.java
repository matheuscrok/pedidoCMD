package com.service.apicmdpedido.utilidades.excecoes;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NumeroControleExistenteException.class)
    public ResponseEntity<Object> handleNumeroControleExistenteException(NumeroControleExistenteException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DetalhePedidoInvalidoException.class)
    public ResponseEntity<Object> handleDetalhePedidoInvalidoException(DetalhePedidoInvalidoException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}

