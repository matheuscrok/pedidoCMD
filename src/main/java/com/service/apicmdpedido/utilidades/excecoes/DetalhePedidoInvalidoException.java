package com.service.apicmdpedido.utilidades.excecoes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalhePedidoInvalidoException extends RuntimeException {
    public DetalhePedidoInvalidoException(String message) {
        super(message);
    }

}