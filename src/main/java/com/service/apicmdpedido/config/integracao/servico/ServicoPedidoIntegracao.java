package com.service.apicmdpedido.config.integracao.servico;


import com.service.apicmdpedido.config.integracao.entidade.DetalhePedidoIntegracao;
import com.service.apicmdpedido.config.integracao.entidade.PedidoIntegracao;
import com.service.apicmdpedido.config.integracao.repositorio.PedidoIntegracaoRepository;
import com.service.apicmdpedido.entidade.Pedido;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ServicoPedidoIntegracao {

    private final PedidoIntegracaoRepository pedidoIntegracaoRepository;

    public ServicoPedidoIntegracao(PedidoIntegracaoRepository pedidoIntegracaoRepository) {
        this.pedidoIntegracaoRepository = pedidoIntegracaoRepository;
    }

    public void salvarPedido(Pedido pedido) {
        pedidoIntegracaoRepository.save(
                new PedidoIntegracao(pedido.getNumeroControle(), pedido.getDataCadastro(), pedido.getCodigoCliente(), pedido.getValorTotal(), pedido.getDetalhes().stream()
                        .map(d -> new DetalhePedidoIntegracao(d.getId(), d.getQuantidade() != null ? d.getQuantidade() : 1, d.getPrecoUnitario(), d.getNomeProduto(), pedido.getNumeroControle()))
                        .collect(Collectors.toList())));
    }

}
