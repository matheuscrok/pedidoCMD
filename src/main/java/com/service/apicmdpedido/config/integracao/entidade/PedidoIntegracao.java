package com.service.apicmdpedido.config.integracao.entidade;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "pedido", schema = "microservico_pedido_query", catalog = "microservico_pedido_query")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PedidoIntegracao {

    @Id
    private Long numeroControle;

    @Column(nullable = false)
    private LocalDate dataCadastro;

    @Column(nullable = false)
    private Long codigoCliente;

    @Column(nullable = false, precision = 10, scale = 2)
    private BigDecimal valorTotal;

    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DetalhePedidoIntegracao> detalhes;
}

