package com.service.apicmdpedido.config.integracao.repositorio;


import com.service.apicmdpedido.config.integracao.entidade.PedidoIntegracao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoIntegracaoRepository extends JpaRepository<PedidoIntegracao, Long> {
}
