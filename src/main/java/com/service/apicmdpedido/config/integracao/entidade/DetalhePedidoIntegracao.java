package com.service.apicmdpedido.config.integracao.entidade;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "detalhe_pedido", schema = "microservico_pedido_query", catalog = "microservico_pedido_query")
public class DetalhePedidoIntegracao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false, precision = 10, scale = 2)
    private BigDecimal precoUnitario;

    @Column(nullable = false, length = 255)
    private String nomeProduto;

    @Column(nullable = false)
    private Long pedido;
}
