package com.service.apicmdpedido.config.fallback;

import com.service.apicmdpedido.config.integracao.servico.ServicoPedidoIntegracao;
import com.service.apicmdpedido.entidade.Pedido;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class FallbackService {
    private static final Logger log = LoggerFactory.getLogger(FallbackService.class);
    private ServicoPedidoIntegracao servicoPedidoIntegracao;

    public FallbackService(ServicoPedidoIntegracao servicoPedidoIntegracao) {
        this.servicoPedidoIntegracao = servicoPedidoIntegracao;
    }

    public void enviarPedido(Pedido pedido) {
        try {
            servicoPedidoIntegracao.salvarPedido(pedido);
        } catch (Exception e) {
            log.error("Erro ao enviar pedido para o banco de dados: " + e.getMessage(), e);
        }
    }


}


