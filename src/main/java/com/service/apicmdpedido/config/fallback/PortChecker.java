package com.service.apicmdpedido.config.fallback;

import java.net.Socket;
import java.io.IOException;

public class PortChecker {

    private PortChecker() {
    }
    public static boolean isPortInUse(String host, int port) {
        try (Socket socket = new Socket(host, port)) {
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
